// Povratna sprega po ugaonoj brzini

// Moram imati klasu koja prestavlja paket upravljanja pod nazivom Actuation
class Actuation
{
  float Ur = 0; // Rotor voltage [V]
}

// Moram imati klasu koja prestavlja merni rezultat pod nazivom Measurement
class Measurement
{
  float w = 0; // Angular velocity [rad/s]
}

int speedo_x;
int speedo_y;


PGraphics pg;
PGraphics speedo;

// Moja simulacija mora da nasledi klasu Simulation i da implementira
// metode Update, Measure, Control, Visualize
class MojaSimulacija extends Simulation
{

  // Potrebni parametri za numeričku simulaciju
  float dt = 0;

  float ddth = 0;
  float dth = 0;
  float th = 0;

  float di = 0;
  float i = 0;

  int n;

  // Parametri modela motora 
  float J = 1; // Moment intercije 
  float Ki = 1; // Strujno-Mehanička karakteristika
  float Ke = 1; // Mehaničko-Strujna karakteristika
  float L = 1; // Induktivnost motora 
  float R = 1; // Otpornost motora
  float b = 1; // Viskozni koeficijent trenja

  // P.F. Sistema je
  //
  //   Ω              Ki
  //  --- = -----------------------
  //   V    (Js + b)(R + Ls) + KeKi

  // Step pobuda
  float step_volt = 20;
  float G = Ki/(b*R + Ke*Ki);
  float step_inf = step_volt * G;

  // Parametri prikaza
  float plot_range = step_inf;
  float speedo_x;
  float speedo_y;

  // Parametri upravljanja
  // PID regulator
  float P = 4;
  float D = 0.1;
  float I = 3;
  float ie = 0;
  float de = 0;
  float prev_e = 0;
  float e;
  float max_volt = 0;
  
  // Refernca
  float Ref = 0;
  

  // Constructor
  MojaSimulacija ()
  {
    // Podesim frekvenciju update-jta simulacije da bude 10 puta veca od
    // frame rate-jta (30 fps)
    SetUpdateMul(100);

    // Podesim periodu kontrolera tako da bude 5 puta manja od perioda simulacije
    SetControlMul(5);

    dt = GetUpdatePeriod();
  }

  // Napravim korak simulacije. Npr. uradim numericku integraciju diferencijalne jednacine.
  // Dato mi je i upravljanje koje je izgenerisao kontroler.
  void Update(Actuation input)
  {
    // Integrating w
    ddth = (1/J)*(Ki*i - b*dth);
    dth += ddth * dt;
    th += dth * dt;

    // Integrating i
    di = (1/L)*(input.Ur - Ke*dth - R*i);
    i += di*dt;

    
  }

  // Napravim merenja (merenja se uvek desava pre izvrsavanja kontrolera)
  Measurement Measure()
  {
    Measurement m = new Measurement();
    m.w = dth + ( randomGaussian() * (PI/50) ); 
    return m;
  }

  // Izracunam novo upravljanje na osnovu merenja
  Actuation Control(Measurement m)
  {
    Actuation o = new Actuation();
    
    e = 8.0*(Ref - m.w);
    
    de = (e - prev_e)/dt;
    ie += e * dt;
    
    prev_e = e;
    
    float k = P*e + D*de + I*ie;
        
    
    if (k > max_volt) max_volt = k;
    
    if (k > 100) k = 100;
    if (k < -100) k = -100;
    
    println(k);
    
    
    
    o.Ur = k;
    
    return o;
  }

  // Vizualizujem rezultat simulacije (frame rate je 30 fps)
  void Visualize()
  {

    // Graph

    pg.beginDraw();
    pg.fill(255, 0, 0);
    //background(150);

    pg.stroke(255, 0, 0);
    //line(0, 360-step_soln, width, 360-step_soln); 
    pg.noStroke();
    //line(0, 360-floor(w), width, 360-floor(w));

    n = n + 1;
    pg.ellipse(n % width, 1.0*height-floor(100*dth/plot_range), 5.0, 5.0);
    pg.endDraw();

    // Image main plot
    stroke(0);
    background(225);

    strokeWeight(1);
    ellipse(width/4, height/4, 100, 100);
    ellipse(width/4, height/4, 20, 20);
    strokeWeight(5);
    line(width/4, height/4, width/4 + 100*cos(th), height/4 + 100*sin(th) );
    image(pg, 0, 0); 

    //
    speedo.beginDraw();
    speedo.background(255);
    speedo.fill(0);
    speedo.noStroke();
    speedo.ellipse(100, 100, 200, 200);
    speedo.fill(255, 0, 0);
    speedo.stroke(255, 0, 0);
    speedo.line(100, 100, 100 - 100*cos(dth/plot_range * 3/4*PI), 100 - 100*sin(dth/plot_range * 3/4* PI));
    speedo.stroke(0, 255, 255);
    speedo.line(100, 100, 100 - 100*cos(Ref/plot_range * 3/4*PI), 100 - 100*sin(Ref/plot_range * 3/4* PI));
    speedo.stroke(255, 255, 0);
    speedo.line(100, 100, 100 - 100*cos(e/plot_range * 3/4*PI), 100 - 100*sin(e/plot_range * 3/4* PI));
    speedo.endDraw();

    speedo_x = 3*width/4 - 100;
    speedo_y = height/4 - 50;

    image(speedo, speedo_x, speedo_y);
    
    // Choose step_volt
    /*if ((mouseX > speedo_x && mouseX < speedo_x + 200 && mouseY > speedo_y && mouseY < speedo_y + 200) &&
    mousePressed)
    {
      step_volt = (PI + atan2(+mouseY - (speedo_y + 100), mouseX - (speedo_x + 100) ))
                  /(3*PI/4) * plot_range / G;
                  
    }*/
    
    //Choose ref.
    if ((mouseX > speedo_x && mouseX < speedo_x + 200 && mouseY > speedo_y && mouseY < speedo_y + 200) &&
    mousePressed)
    {
          Ref = (PI + atan2(+mouseY - (speedo_y + 100), mouseX - (speedo_x + 100) ))
                  /(3*PI/4) * plot_range;
          ie = 0;
                 
    }
                 
    
  }
}

// Moram implementirati funkciju koja inicijalizuje simulaciju
// i u njoj postaviti globalnu promenljivu sim na instancu moje simulacije
void InitSimulation()
{
  surface.setSize(800, 600);
  pg = createGraphics(800, 600); 
  speedo = createGraphics(200, 100); 

  stroke(255); 
  sim = new MojaSimulacija();
}