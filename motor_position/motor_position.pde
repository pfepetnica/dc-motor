// Povratna sprega po položju brzini

// Moram imati klasu koja prestavlja paket upravljanja pod nazivom Actuation
class Actuation
{
  float Ur = 0; // Rotor voltage [V]
}

// Moram imati klasu koja prestavlja merni rezultat pod nazivom Measurement
class Measurement
{
  float th = 0; // Current angle [rad]
}

int speedo_x;
int speedo_y;


PGraphics pg;
PGraphics speedo;

// Moja simulacija mora da nasledi klasu Simulation i da implementira
// metode Update, Measure, Control, Visualize
class MojaSimulacija extends Simulation
{

  // Potrebni parametri za numeričku simulaciju
  float dt = 0;

  float ddth = 0;
  float dth = 0;
  float th = 0;

  float di = 0;
  float i = 0;

  int n;

  // Parametri modela motora 
  float J = 1; // Moment intercije 
  float Ki = 1; // Strujno-Mehanička karakteristika
  float Ke = 1; // Mehaničko-Strujna karakteristika
  float L = 1; // Induktivnost motora 
  float R = 1; // Otpornost motora
  float b = 1; // Viskozni koeficijent trenja

  // P.F. Sistema je
  //
  //   A             Ki s
  //  --- = -----------------------
  //   V    (Js + b)(R + Ls) + KeKi

  // Step pobuda
  float step_volt = 20;
  float G = Ki/(b*R + Ke*Ki);
  float step_inf = step_volt * G;

  // Parametri prikaza
  float plot_range = step_inf;
  float speedo_x;
  float speedo_y;

  // Parametri upravljanja
  // PID regulator
  float P = 6;
  float D = 1;
  float I = 9.25;
  float ie = 0;
  float de = 0;
  float dde = 0;
  float prev_e = 0;
  float prev_k = 0;
  float prev_de;
  float prev_dde;
  float e;
  float max_volt = 0;
  
  // Refernca
  float Ref = 0;
  

  // Constructor
  MojaSimulacija ()
  {
    // Podesim frekvenciju update-jta simulacije da bude 10 puta veca od
    // frame rate-jta (30 fps)
    SetUpdateMul(10);

    // Podesim periodu kontrolera tako da bude 5 puta manja od perioda simulacije
    SetControlMul(5);

    dt = GetUpdatePeriod();
  }

  // Napravim korak simulacije. Npr. uradim numericku integraciju diferencijalne jednacine.
  // Dato mi je i upravljanje koje je izgenerisao kontroler.
  void Update(Actuation input)
  {
    // Integrating th
    println(i);
    
    float I = 2.0; // Bottom limit current
    
    if (abs(i) <= I) //Nonlinear effect /-/ 
    {
      ddth = (1/J)*(-b*dth);
    }
    else
    if (i > 0.100) 
    {
      ddth = (1/J)*(Ki*(i-I)- b*dth);
    }
    else 
    {
      ddth = (1/J)*(Ki*(i+I)- b*dth);
    }
    
    dth += ddth * dt;
    th += dth * dt;

    // Integrating i
    di = (1/L)*(input.Ur - Ke*dth - R*i);
    i += di*dt;
  }

  // Napravim merenja (merenja se uvek desava pre izvrsavanja kontrolera)
  Measurement Measure()
  {
    Measurement m = new Measurement();
    m.th = th + randomGaussian() * (PI/100);
    // Kalman Filtering of model
    
    return m;
  }

  // Izracunam novo upravljanje na osnovu merenja
  Actuation Control(Measurement m)
  {
    Actuation o = new Actuation();
    
    e = 10*(Ref - m.th);  
   
    de = (e - prev_e)/dt;
    
    prev_e = e;
   
    float k = de + e;
   
    //if (k > 100)  k = 100;
    //if (k < -100) k = -100;
     
    //println(k);
     
    o.Ur = k;
    
    return o;
  }

  // Vizualizujem rezultat simulacije (frame rate je 30 fps)
  void Visualize()
  {

    // Graph

    pg.beginDraw();
    pg.fill(255, 0, 0);
    //background(150);

    pg.stroke(255, 0, 0);
    //line(0, 360-step_soln, width, 360-step_soln); 
    pg.noStroke();
    //line(0, 360-floor(w), width, 360-floor(w));

    n = n + 1;
    pg.ellipse(n, 1.0*height-floor(100*th/2/PI), 2.0, 2.0);
    pg.endDraw();

    // Image main plot
    stroke(0);
    background(225);
  
    text(n*dt, 10, 10);
    strokeWeight(1);
    ellipse(width/4, height/4, 100, 100);
    ellipse(width/4, height/4, 20, 20);
    strokeWeight(5);
    line(width/4, height/4, width/4 + 100*cos(th), height/4 + 100*sin(th) );
    image(pg, 0, 0); 

    //
    speedo.beginDraw();
    speedo.background(255);
    speedo.fill(0);
    speedo.noStroke();
    speedo.ellipse(100, 100, 200, 200);
    speedo.fill(255, 0, 0);
    speedo.stroke(255, 0, 0);
    speedo.line(100, 100, 100 - 100*cos(th), 100 - 100*sin(th));
    speedo.stroke(0, 255, 255);
    speedo.line(100, 100, 100 - 100*cos(Ref), 100 - 100*sin(Ref));
    speedo.stroke(255, 255, 0);
    speedo.line(100, 100, 100 - 100*cos(e/10), 100 - 100*sin(e/10));
    speedo.endDraw();

    speedo_x = 3*width/4 - 100;
    speedo_y = height/4 - 50;

    image(speedo, speedo_x, speedo_y);
    
    
    //Choose ref.
    if ((mouseX > speedo_x && mouseX < speedo_x + 200 && mouseY > speedo_y && mouseY < speedo_y + 200) &&
    mousePressed)
    {
          Ref = (PI + atan2(-(speedo_y + 100) + mouseY, -(speedo_x + 100) + mouseX));
          ie = 0;
    }                
    
  }
}

// Moram implementirati funkciju koja inicijalizuje simulaciju
// i u njoj postaviti globalnu promenljivu sim na instancu moje simulacije
void InitSimulation()
{
  surface.setSize(800, 600);
  pg = createGraphics(800, 600); 
  speedo = createGraphics(200, 200); 

  stroke(255); 
  sim = new MojaSimulacija();
}